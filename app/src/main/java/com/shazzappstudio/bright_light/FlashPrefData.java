package com.shazzappstudio.bright_light;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class FlashPrefData {
    static final String PREF_STORE = "mPref";

    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setVal(Context ctx, boolean str) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putBoolean(PREF_STORE, str);
        editor.commit();
    }

    public static boolean getVal(Context ctx) {
        return getSharedPreferences(ctx).getBoolean(PREF_STORE, false);
    }
}