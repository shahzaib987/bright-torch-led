package com.shazzappstudio.bright_light;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

public class FlashService extends Service {
    private boolean isFlashOn = false;
    static Camera camera;
    Camera.Parameters p;
    Context context;
    PackageManager pm;
    MediaPlayer mp;
   // private static final String ACTION_STOP_REQUEST = FlashService.class.getName() + ".ACTION_STOP_REQUEST";
    private static final int SERVICE_ID = 72890;

    @Override
    public void onCreate() {
        context = getApplicationContext();
        super.onCreate();
        mp = MediaPlayer.create(this, R.raw.click);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        pm = context.getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(getApplicationContext(),
                    "Please upgrade your phone's hardware!! " +
                            "Camera Flashlight wont work " +
                            "in this phone.", Toast.LENGTH_SHORT)
                    .show();
            return 0;
        }
        mp.start();
        boolean turnFlashOn = FlashPrefData.getVal(context);
        updateWidget(turnFlashOn);
        if (turnFlashOn) {
            camera = Camera.open();
            Camera.Parameters param = camera.getParameters();
            param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            try {
                camera.setParameters(param);
                camera.startPreview();
                startForeground(SERVICE_ID, buildRunningNotification());
              //  isLightOn = true;
            } catch (Exception e) {
                Toast.makeText(context, "No Flash Light Found !!", Toast.LENGTH_SHORT).show();
            }

        } else {
           if(camera!=null){
               camera.stopPreview();
               camera.release();
               camera = null;
               stopForeground(true);


           }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    void updateWidget(boolean on) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
        ComponentName thisWidget = new ComponentName(context, TorchWidget.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        if (appWidgetIds != null && appWidgetIds.length > 0) {
            for (int widgetId : appWidgetIds) {
                RemoteViews rViews = new RemoteViews(context.getPackageName(), R.layout.flash_wiget_layout);
                rViews.setImageViewResource(R.id.widget_button, (on ? R.drawable.bulb_on : R.drawable.bulb_off));
                appWidgetManager.updateAppWidget(widgetId, rViews);
            }
        }
    }

    void stopLight()
    {
        if(camera!=null){
            camera.stopPreview();
            camera.release();
            camera = null;
            stopForeground(true);
        }
    }

    private Notification buildRunningNotification() {
        Intent stopService = new Intent(getApplicationContext(), FlashActivity.class);
       // stopService.setAction(ACTION_STOP_REQUEST);
       // stopService.setAction(startActivity());
      //  PendingIntent pIntent = PendingIntent.getService(getApplicationContext(), 0,  FlashActivity.class, 0);
        stopService.setAction(Long.toString(System.currentTimeMillis()));
        //new Intent(); // add this
        //	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, stopService, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setContentTitle("Bright Torch LED");
        builder.setContentText("Touch to power off flash light");
        builder.setSmallIcon(R.drawable.bulb_off);
        builder.setContentIntent(pIntent);
        Notification runningNotification = builder.build();
        return runningNotification;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}