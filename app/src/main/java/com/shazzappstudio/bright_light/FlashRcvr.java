package com.shazzappstudio.bright_light;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class FlashRcvr extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, FlashService.class);
        final boolean isFlashOn = FlashPrefData.getVal(context);
        // update preference values
        FlashPrefData.setVal(context, !isFlashOn);
        // camera operations in service
        context.startService(serviceIntent);

    }
}
