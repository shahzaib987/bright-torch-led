package com.shazzappstudio.bright_light;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;


/**
 * Implementation of App Widget functionality.
 */
public class TorchWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {

            int currentWidgetId = appWidgetIds[i];
            Intent intent = new Intent(context, FlashRcvr.class);
            PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.flash_wiget_layout);
            views.setImageViewResource(R.id.widget_button, (FlashPrefData.getVal(context) ? R.drawable.bulb_on : R.drawable.bulb_off));
            views.setOnClickPendingIntent(R.id.widget_button, pending);
            appWidgetManager.updateAppWidget(currentWidgetId, views);
        }
    }


}
