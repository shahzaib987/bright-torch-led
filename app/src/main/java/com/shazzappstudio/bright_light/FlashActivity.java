package com.shazzappstudio.bright_light;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import io.fabric.sdk.android.Fabric;

public class FlashActivity extends Activity {
    static private ImageButton flashlightSwitchImg;
    private PublisherAdView mAdView;
    private PublisherAdView mAdView1;
    static Camera camera;
    private PublisherInterstitialAd mPublisherInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_flash);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        ((GoogleAnalyticsBright) getApplication())
                .getTracker(GoogleAnalyticsBright.TrackerName.APP_TRACKER);

        mAdView = (PublisherAdView) findViewById(R.id.ad_view1);

        mAdView1 = (PublisherAdView) findViewById(R.id.ad_view2);

        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);
        mAdView1.loadAd(adRequest);

        mPublisherInterstitialAd = new PublisherInterstitialAd(this);
        // Defined in res/values/strings.xml
        mPublisherInterstitialAd.setAdUnitId(getString(R.string.interstitial_unit));

        PublisherAdRequest.Builder publisherAdRequestBuilder = new PublisherAdRequest.Builder();
        mPublisherInterstitialAd.loadAd(publisherAdRequestBuilder.build());


        flashlightSwitchImg = (ImageButton) findViewById(R.id.bullOnOff);
        flashlightSwitchImg.setImageResource(FlashPrefData.getVal(this) ? R.drawable.bulb_on : R.drawable.bulb_off);
        flashlightSwitchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPublisherInterstitialAd.show();
                final boolean isFlashOn = FlashPrefData.getVal(FlashActivity.this);
                FlashPrefData.setVal(FlashActivity.this, !isFlashOn);
                //update icon as per preference values
                flashlightSwitchImg.setImageResource(FlashPrefData.getVal(FlashActivity.this) ? R.drawable.bulb_on : R.drawable.bulb_off);
                // Start service
                Intent bgCamService = new Intent(FlashActivity.this, FlashService.class);
                startService(bgCamService);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);

    }

    @Override
    public void onBackPressed() {


        this.finish();
        moveTaskToBack(true);
        System.exit(0);


    }
}