package com.shazzappstudio.bright_light;

import android.app.Application;

/**
 * Created by Prinay Agrawal on 17-May-15.
 */
public class FlashLight extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlashPrefData.setVal(getApplicationContext(), false);
    }
}
